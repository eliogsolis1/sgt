<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Ticket;
use Redirect;
use Session;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{
    public function index(Request $request){
      //dd($request->get('level'));
      $tickets   = Ticket::level($request->get('level'))->get();
      //$tickets = Ticket::All();
      return view('ticket.index' , compact('tickets'));
    }

    public function create(){
      return view('ticket.create');
    }

    public function store(Request $request){
      $rules = array(
          'name' => 'required|unique:tickets',
          'description' => 'required|min:5',
          'level' => 'required'
      );

      $validator = Validator::make($request->all() , $rules);

      if ($validator->fails()) {
         return redirect('tickets/create')->withErrors($validator)->withInput();
      } else {
        Ticket::create([
          'name'        => $request['name'],
          'description' => $request['description'],
          'level'       => $request['level']
        ]);

        Session::flash('status', 'El ticket ha sido creado satisfactoriamente!!');
        return Redirect::to('/tickets');
      }
    }

    public function edit($id){
      $ticket = Ticket::find($id);
      return view('ticket.edit' , compact('ticket'));
    }

    public function update(Request $request, $id){
      $ticket = Ticket::find($id);
      $ticket->fill($request->all());
      $ticket->save();

      Session::flash('status', 'El ticket ha sido actualizado satisfactoriamente!!');
      return Redirect::to('tickets');
    }

    public function destroy($id){
      Ticket::destroy($id);
      Session::flash('status','El Ticket seleccionado fue Eliminado');
      return Redirect::to('/tickets');
    }
}
