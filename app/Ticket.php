<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';

    protected $fillable = [
      'name' , 'description' , 'level'
    ];

    public function getLevelAttribute($value){
      if($value == 0){
        $valor = 'Bajo';
      }else if($value == 1){
        $valor = 'Medio';
      }elseif ($value == 2) {
        $valor = 'Urgente';
      }
      return $valor;
    }

    public function scopeLevel($query, $level){
      //dd("scope: ".$level);
      if ($level != "") {
        $query->where('level',$level);
      }
    }
}
