<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        {!!Html::style('semantic/semantic.min.css')!!}
        {!!Html::script('semantic/semantic.min.js')!!}
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>SISTEMA GESTION DE TICKETS</h1>
            <a class="ui button" href="{!!URL::to('/tickets')!!}" >Ir a Mantenimiento Ticket</a>

      <h3 class="ui header">
          <img src="{{ asset('/images/batman.jpg') }}" class="ui circular image">
         <div class="content">
           Elio G. Solis
           <div class="sub header">Developer</div>
         </div>
      </h3>
        </div>
    </body>
</html>
