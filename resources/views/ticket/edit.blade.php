@extends('layouts.app')

@section('content')

    <div class="page-header">
      <h1>
        Editar Ticket
        <a class="ui button right floated" href="{!!URL::to('/tickets')!!}" >Cancelar</a>
			  <button id="btn-save-ticket" class="ui primary button right floated">Guardar</button>
      </h1>
    </div>

    {{ Form::model($ticket,['route'=>['tickets.update',$ticket->id] , 'method'=>'PUT', 'id'=>'form-edit-ticket']) }}
		 @include('ticket/forms/form')
	  {{ Form::close() }}

    <script type="text/javascript">
      $('#btn-save-ticket').click(function(){
        $("#form-edit-ticket").submit();
    });
    </script>
@endsection
