@extends('layouts.app')

@section('content')

  <div class="page-header">
        <h1>Listado de tickets
        {{ link_to_route('tickets.create', 'Crear Ticket','' , array('class' => 'ui primary button right floated')) }}
        </h1>
        {!!Html::script('js/buscador.js')!!}
  </div>

  @if(Session::has('status'))
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<p><strong>Acción realizada con Exito</strong> </p>
			<ul>
				<li>{{ Session::get('status') }}</li>
			</ul>
		</div>
	@endif

  <div class="col-sm-3 col-md-3 pull-right">
		<div class="form-group has-feedback">
	    <label class="control-label">Buscador</label>
	    <input type="text" class="form-control" id="buscar" onkeyup="buscador()" placeholder="Buscar..." />
	    <i class="glyphicon glyphicon-search form-control-feedback"></i>
		</div>
	</div>


  {{ Form::open(['route'=>'tickets.index' , 'method'=>'GET' , 'class'=>'pull-left']) }}
      <div class="form-group">
        <label class="sr-only">Nivel de importancia</label>
        <select class="form-control" name="level">
          <option selected="" disabled=""> Seleccionar Nivel</option>
          <option value="0"> Bajo </option>
            <option value="1"> Medio </option>
              <option value="2"> Urgente </option>
        </select>
      </div>
      <button type="submit" class="ui tiny primary button">Filtrar</button>
 {{ Form::close() }}



  <div class="content">

    <table class="table table-hover" id="tickets">
      <thead>
        <th>Nombre</th>
          <th>Descripcion</th>
            <th>Importacia</th>
              <th>Accion</th>
      </thead>
        @foreach($tickets as $ticket)
      <tbody>
          <td> {{ $ticket->name }} </td>
          <td> {{ $ticket->description }} </td>
          <td> {{ $ticket->level }}</td>
          <td>
					<a href="{{ route('tickets.edit', ['id' => $ticket->id]) }}" class="ui green button">Editar</a>
					</td>
					<td>
					{{ Form::open(['route' => ['tickets.destroy', $ticket->id ], 'method'=>'DELETE']) }}
						<button type="submit" class="ui red button" onclick="return confirm('Deseas eliminar este registro?')">Eliminar</button>
					{{ Form::close() }}
					</td>

      </tbody>
      @endforeach
    </table>

  </div>
@endsection
