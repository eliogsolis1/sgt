@extends('layouts.app')

@section('content')
  <div class="page-header">
    <h1>
      Crear Ticket
      <a class="ui button right floated" href="{!!URL::to('/tickets')!!}" >Cancelar</a>
			<button id="btn-save-ticket" class="ui primary button right floated">Guardar</button>
    </h1>
  </div>

  @include('ticket/alerts/errors')

  {{ Form::open(['route'=>'tickets.store','method'=>'POST', 'id'=>'form-create-ticket'])}}
    @include('ticket/forms/form')
  {{ Form::close() }}

  <script type="text/javascript">
    $('#btn-save-ticket').click(function(){
      $("#form-create-ticket").submit();
  });
  </script>
@endsection
