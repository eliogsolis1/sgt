<div class="col-md-6">

    <div class="form-group">
      {{ Form::label('Nombre del Ticket')}}
      {{ Form::text('name',null,['class'=>'form-control']) }}
    </div>

    <div class="form-group">
      {{ Form::label('Descripcion del Ticket')}}
      @if(isset($ticket->description))
        <textarea class="form-control" name="description" rows="4" cols="40">{{ $ticket->description }}</textarea>
        @else
        <textarea class="form-control" name="description" rows="4" cols="40"></textarea>
      @endif

    </div>

    <div class="form-group">
      {{ Form::label('Importancia del Ticket')}}

      @if(isset($ticket->level))
          <select class="form-control" name="level">
        @if($ticket->level == 'Bajo')
            <option value="0" selected="">Bajo</option>
            <option value="1">Medio</option>
            <option value="2">Urgente</option>
        @elseif($ticket->level == 'Medio')
            <option value="0" >Bajo</option>
            <option value="1" selected="">Medio</option>
            <option value="2">Urgente</option>
        @elseif($ticket->level == 'Urgente')
            <option value="0" >Bajo</option>
            <option value="1">Medio</option>
            <option value="2"  selected="" >Urgente</option>
          </select>
        @endif

      @else
        <select class="form-control" name="level">
          <option selected="" disabled="">-- Seleccionar Nivel de importancia --</option>
          <option value="0">Bajo</option>
          <option value="1">Medio</option>
          <option value="2">Urgente</option>
        </select>
      @endif

    </div>
</div>
